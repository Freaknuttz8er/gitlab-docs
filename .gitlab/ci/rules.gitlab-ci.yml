###############################################
#    Job configuration rules and defaults     #
###############################################

default:
  image: registry.gitlab.com/gitlab-org/gitlab-docs/base:alpine-3.16-ruby-3.0.5-869cfc5d
  tags:
    - gitlab-org

.bundle:
  before_script:
    - ruby --version
    - gem --version
    - bundle --version
    - bundle config set --local deployment true  # Install dependencies into ./vendor/bundle
    - bundle install --jobs 4
  cache:
    key:
      files:
        - Gemfile.lock
    paths:
      - vendor/bundle

.bundle_and_yarn:
  before_script:
    - ruby --version
    - gem --version
    - bundle --version
    - bundle config set --local deployment true  # Install dependencies into ./vendor/bundle
    - bundle install --jobs 4
    - node --version
    - yarn --version
    - yarn install --frozen-lockfile --cache-folder .yarn-cache
  cache:
    key:
      files:
        - Gemfile.lock
        - yarn.lock
    paths:
      - vendor/bundle
      - .yarn-cache/

.yarn:
  before_script:
    - node --version
    - yarn --version
    - yarn install --frozen-lockfile --cache-folder .yarn-cache
  cache:
    key:
      files:
        - yarn.lock
    paths:
      - .yarn-cache/

#
# Retry a job automatically if it fails (2 times)
#
.retry:
  retry: 2

#
# Rules to determine which pipelines jobs will run in.
#
.rules_scheduled:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: never
    - if: '$PIPELINE_SCHEDULE_TIMING == "weekly"'
    - if: '$PIPELINE_SCHEDULE_TIMING == "hourly"'
      when: manual
      allow_failure: true
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
      allow_failure: true
    - if: '$CI_COMMIT_BRANCH == "main"'
      when: manual
      allow_failure: true

.rules_scheduled_manual:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: manual
      allow_failure: true

.rules_chores:
  rules:
    - if: '$CLEAN_REVIEW_APPS_DAYS'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CHORES_PIPELINE == "true"'
      when: manual
      allow_failure: true

.rules_site_tests:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    # Don't run site tests for review apps.
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_MERGE_REQUEST_ID'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

.rules_global_nav_test:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_MERGE_REQUEST_ID'
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

.rules_prod:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    # Don't deploy to production for trigerred pipelines (usually review apps)
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger"'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$CI_COMMIT_BRANCH =~ /^\d{1,2}\.\d{1,2}$/'

.rules_dev:
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_COMMIT_REF_NAME =~ /algolia/
      variables:
        SEARCH_BACKEND: 'algolia'
    - if: $CI_MERGE_REQUEST_ID && $CI_COMMIT_REF_NAME =~ /gps/
      variables:
        SEARCH_BACKEND: 'google'
    - if: '$CI_MERGE_REQUEST_ID'
    - if: '$CI_COMMIT_BRANCH =~ /docs-preview/'  # TODO: Remove once no projects create such branch
    - if: '$CI_PIPELINE_SOURCE == "schedule" && $CHORES_PIPELINE == "true"'

.rules_upstream_review_app:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "pipeline" || $CI_PIPELINE_SOURCE == "trigger"'

.rules_pages:
  rules:
    - if: $CHORES_PIPELINE == "true" || $CLEAN_REVIEW_APPS_DAYS
      when: never
    # Don't deploy to production for trigerred pipelines (usually review apps)
    - if: '$CI_PIPELINE_SOURCE == "pipeline"|| $CI_PIPELINE_SOURCE == "trigger"'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
